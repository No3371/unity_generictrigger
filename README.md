# Unity_GenericCallbacks

Several simple components allow you to configure all the collision/trigger event in inspector just to speed up your level/content development/prototyping, or for our laziness :p

## Usage
Just add component you need to any GameObject you want.

- **GenericCallbacks**: It get called by Awake(), Start(), Update()...
- **GenericTrigger(/2D)** : It get called by Unity's trigger callbacks.
- **GenericCollider(/2D)** : It get called by Unity's collsion callbacks.
- **Adv_GenericTrigger2D** : This is a more customized one made for my previous 2D game. It allows you to assure the actual position of the transform of the triggering object is inside the trigger zone.

## Dependency
- Collision/Trigger have to work with Unity's colldiers.
- This plugin use my other plugin *GameObjectFilter*, which is a utility class allow you to quick filter Gameobjects by tag or assigned instances.
- This plugin use my other plugin *CommonUnityEvents*, which is just a bunch of predefined serializable UnityEvent<T> classes.